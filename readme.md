# aes-classloader-maven-plugin

A simple maven plugin to encrypt the jar classes with AES.

## Warning
Hardcoding a password in a POM directly can be disastrous, since Maven add the descriptor inside the generated jar (a copy of the pom).
<br>`/META-INF/maven/[groupId]/[artifactId]/pom.xml`

I highly suggest that you use a system variable and use it in the POM.

If you don't want to do that, you can also disable the descriptor by using the `maven-jar-plugin`.

But, for the descriptor, nothing stops another plugin of adding it again.
```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-jar-plugin</artifactId>
    <version>LATEST</version>
    <configuration>
        <archive>
            <addMavenDescriptor>false</addMavenDescriptor>
        </archive>
    </configuration>
</plugin>
```

## Available plugin goals

The `key` and `keyB64` represent the key in two different ways.
* `key`    | Represent the raw key (not encoded)
* `keyB64` | Represent the key in base64 (recommended)

You cannot add both parameters.

If none of them is specified, a random key will be generated and saved at the root of the project in a file called `aes-classloader.key`.

Keep in mind that you WILL need to un-base64 the key before passing it to the classloader. 

### encrypt-resources-with-aes
The goal encrypts the resources with AES (CBC).

#### Parameters
|         Name          |  Required  |               Description                                                             |
|-----------------------|:----------:|:-------------------------------------------------------------------------------------:|
| key                   | false      | The key that will be used to encrypt / decrypt                                        |
| keyB64                | false      | The key that will be used to encrypt / decrypt represented in base 64                 |

**Example**
```xml
<plugin>
    <groupId>ca.watier.maven.plugins</groupId>
    <artifactId>aes-classloader-maven-plugin</artifactId>
    <configuration>
        <key>test</key> <!-- The key / password to be used -->
    </configuration>
    <executions>
        <execution>
            <phase>prepare-package</phase> <!-- Default phase -->
            <goals>
                <goal>encrypt-resources-with-aes</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

### encrypt-classes-with-aes
The goal encrypts the compiled classes with AES (CBC).

#### Parameters
|         Name          |  Required  |               Description                                                             |
|-----------------------|:----------:|:-------------------------------------------------------------------------------------:|
| key                   | false      | The key that will be used to encrypt / decrypt the classes                            |
| keyB64                | false      | The key that will be used to encrypt / decrypt represented in base 64                 |
| skipClassesByRegex    | false      | Skip the specified entry by regex (file, path, ect)                                   |
| skipClassesByType     | false      | Skip the specified entry by class type (INTERFACES, ENUMS & ANNOTATIONS)              |

**Example**
```xml
<plugin>
    <groupId>ca.watier.maven.plugins</groupId>
    <artifactId>aes-classloader-maven-plugin</artifactId>
    <configuration>
        <key>test</key> <!-- The key / password to be used -->
        <encryptResourceFolder>true</encryptResourceFolder> <!-- The default is false -->
        <skipClassesByRegex>
            <class>App.class</class>
        </skipClassesByRegex>
        <skipClassesByType>
            <type>INTERFACES</type>
            <type>ENUMS</type>
        </skipClassesByType>
    </configuration>
    <executions>
        <execution>
            <phase>prepare-package</phase> <!-- Default phase -->
            <goals>
                <goal>encrypt-classes-with-aes</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

### generate-classloader-source
The goal adds the classloader java source to the `generated-sources` folder.

To compile the classloader, add the following plugin to the POM.
```xml
<plugin>
    <groupId>org.codehaus.mojo</groupId>
    <artifactId>build-helper-maven-plugin</artifactId>
    <version>3.2.0</version>
    <executions>
        <execution>
            <phase>generate-sources</phase>
            <goals>
                <goal>add-source</goal>
            </goals>
            <configuration>
                <sources>
                    <source>${project.build.directory}/generated-sources/</source>
                </sources>
            </configuration>
        </execution>
    </executions>
</plugin>
```

## Documentation

### Required Java version for the classloader
8+

### Encrypted files 

#### *.enc
* The first 16 bytes is the IV; the rest is the class data.

#### *.resenc
* The first 16 bytes is the IV; the rest is the resource data (as a zip).

#### aes-classloader.key
* This file is generated when no key is specified in the pom; the key inside the file is encoded in base64 and can be uses directly in the `ca.watier.maven.plugins.aes.AesClassLoader`