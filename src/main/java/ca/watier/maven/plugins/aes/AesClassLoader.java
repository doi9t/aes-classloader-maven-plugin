/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins.aes;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.*;
import java.util.jar.JarInputStream;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * WARNING: Make sure that this class doesn't use external libraries; since we inject it in other jars.
 */
public class AesClassLoader extends ClassLoader {

    private static final String PROTOCOL_PATTERN = "%s:";
    private static final Pattern REGEX_BINARY_NAME = Pattern.compile(".+\\..+");
    private static final String ROOT_URL;
    private static final boolean IS_JAR;

    static {
        URL resource = AesClassLoader.class.getResource("/ca"); // Since we know this class uses the "ca" package in the jar

        if (resource == null) {
            ROOT_URL = null;
            IS_JAR = false;
        } else {
            String protocol = resource.getProtocol();
            String urlAsString = resource.toExternalForm();
            urlAsString = removeProtocol(urlAsString, protocol);
            urlAsString = removeProtocol(urlAsString, "file");

            IS_JAR = protocol.equalsIgnoreCase("jar");
            ROOT_URL = urlAsString.replaceFirst(generateContextPath(), "");
        }
    }

    private final Map<String, Class<?>> classCache = new HashMap<>();
    private final Map<String, byte[]> resourceCache = new HashMap<>();

    //<editor-fold desc="Variables templates">
    @SuppressWarnings("unused")
    private String saltString;
    @SuppressWarnings("unused")
    private int iterationCount;
    @SuppressWarnings("unused")
    private int keyLength;
    @SuppressWarnings("unused")
    private String keyAlgorithm;
    @SuppressWarnings("unused")
    private String transformation;
    @SuppressWarnings("unused")
    private String encryptedClassExt;
    @SuppressWarnings("unused")
    private String resourcesFilePath;
    //</editor-fold>

    /**
     * @param decryptionKey - The decryption key that will be used to decrypt the files.
     * @throws NoSuchAlgorithmException - if no Provider supports the current algorithm.
     * @throws InvalidKeySpecException  - If the key spec is invalid.
     * @throws IllegalStateException    - When the decryption is failing or the context is not valid (Unable to find the salt file, the provided `decryptionKey` is invalid, ect).
     */
    public AesClassLoader(String decryptionKey) throws NoSuchAlgorithmException, InvalidKeySpecException, IllegalStateException {
        if (decryptionKey == null || decryptionKey.isEmpty()) {
            throw new IllegalStateException("The decryption key cannot be null or empty!");
        }

        if (saltString == null || saltString.isEmpty()) {
            throw new IllegalStateException("The salt cannot be null or empty!");
        }

        final SecretKey secretKey = buildSecretKey(decryptionKey);

        try {
            loadEncryptedClassIfNeeded(secretKey);
            loadEncryptedResourceIfNeeded(secretKey);
        } catch (IOException | NoSuchAlgorithmException | InvalidKeyException |
                InvalidAlgorithmParameterException | NoSuchPaddingException | BadPaddingException |
                IllegalBlockSizeException e) {
            throw new IllegalStateException("Unable to load the classes!", e);
        }
    }

    private static String generateContextPath() {
        if (IS_JAR) {
            return "!/ca";
        }

        return "/ca";
    }

    private static String removeProtocol(String resource, String protocol) {
        if ((resource == null || resource.isEmpty()) || (protocol == null || protocol.isEmpty())) {
            return "";
        }

        return resource.replace(String.format(PROTOCOL_PATTERN, protocol), "");
    }

    private void loadEncryptedClassIfNeeded(SecretKey secretKey) throws IOException, NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        if (IS_JAR) {
            readEncryptedFilesFromJar(secretKey);
        } else {
            readEncryptedFilesFromDir(secretKey);
        }
    }

    private void loadEncryptedResourceIfNeeded(SecretKey secretKey) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        if (resourcesFilePath == null) {
            return;
        }

        try (InputStream resourceFileStream = AesClassLoader.class.getResourceAsStream(resourcesFilePath)) {

            if (resourceFileStream == null) {
                return;
            }

            final byte[] rawFileBytes = readAllBytes(resourceFileStream);

            byte[] iv = new byte[16];
            System.arraycopy(rawFileBytes, 0, iv, 0, iv.length);

            byte[] encryptedFileData = new byte[rawFileBytes.length - iv.length];
            System.arraycopy(rawFileBytes, iv.length, encryptedFileData, 0, encryptedFileData.length);

            final byte[] decryptedFile = decrypt(iv, secretKey, encryptedFileData);
            try (ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(decryptedFile))) {
                ZipEntry ze;
                while ((ze = zis.getNextEntry()) != null) {
                    resourceCache.put(ze.getName(), readAllBytes(zis));
                }
            }
        }
    }

    private byte[] readAllBytes(InputStream is) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[4096];

        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        return buffer.toByteArray();
    }

    private SecretKey buildSecretKey(String decryptionKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory factory = SecretKeyFactory.getInstance(keyAlgorithm);
        KeySpec spec = new PBEKeySpec(decryptionKey.toCharArray(), Base64.getDecoder().decode(saltString), iterationCount, keyLength);
        return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
    }

    private void readEncryptedFilesFromDir(SecretKey secretKey) throws IOException, NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
        List<Path> classes = Files.walk(Paths.get(ROOT_URL))
                .filter(p -> p.toString().endsWith(encryptedClassExt))
                .collect(Collectors.toList());

        for (Path currentPath : classes) {
            String fileName = currentPath.getFileName().toString();
            byte[] fileContent = Files.readAllBytes(currentPath);
            handleEncryptedFile(fileName, fileContent, secretKey);
        }
    }

    private void readEncryptedFilesFromJar(SecretKey secretKey) throws IOException, NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        InputStream inputStream = new FileInputStream(new File(ROOT_URL));
        JarInputStream jarInputStream = new JarInputStream(inputStream);

        ZipEntry entry;
        while ((entry = jarInputStream.getNextEntry()) != null) {
            String name = entry.getName();
            handleZipEntry(name, jarInputStream, secretKey);
        }
    }

    private void handleZipEntry(String name, JarInputStream jarInputStream, SecretKey secretKey) throws IOException, NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        if (!name.endsWith(encryptedClassExt)) {
            return;
        }

        handleEncryptedFile(name, readAllBytes(jarInputStream), secretKey);
    }

    private void handleEncryptedFile(String name, byte[] rawClassBytes, SecretKey secretKey) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
        if (rawClassBytes == null || rawClassBytes.length < 17 || name == null) {
            return;
        }

        byte[] iv = new byte[16];
        System.arraycopy(rawClassBytes, 0, iv, 0, iv.length);

        byte[] encryptedClassData = new byte[rawClassBytes.length - iv.length];
        System.arraycopy(rawClassBytes, iv.length, encryptedClassData, 0, encryptedClassData.length);

        byte[] decryptedClassBytes = decrypt(iv, secretKey, encryptedClassData);

        String binaryName = extractBinaryName(name);
        Class<?> clazz = defineClass(binaryName, decryptedClassBytes, 0, decryptedClassBytes.length);
        classCache.put(binaryName, clazz);
    }

    private byte[] decrypt(byte[] iv, SecretKey secretKey, byte[] ciphertext) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
        return cipher.doFinal(ciphertext);
    }

    private String extractBinaryName(String name) {
        if (name == null || name.isEmpty()) {
            return "";
        }

        String[] split = name.split("/");
        return String.join(".", split).replaceFirst(encryptedClassExt, "");
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {

        Class<?> aClass;
        if (isClassWithBinaryName(name)) {
            aClass = classCache.get(name);
        } else {
            List<String> foundKeys = filterClassNameFromClassCache(name);
            int numberOfFoundKeys = foundKeys.size();

            if (numberOfFoundKeys == 0) {
                throw new ClassNotFoundException("No class found without the binary name of the class; you need to specify it or enter a valid name!");
            } else if (numberOfFoundKeys > 1) {
                throw new ClassNotFoundException("Too many classes found without the binary name of the class; you need to specify it!");
            }

            aClass = classCache.get(foundKeys.get(0));
        }

        if (aClass != null) {
            return aClass;
        }

        return super.loadClass(name);
    }

    private boolean isClassWithBinaryName(String name) {
        if (name == null || name.isEmpty()) {
            return false;
        }

        return REGEX_BINARY_NAME.matcher(name).find();
    }

    private List<String> filterClassNameFromClassCache(String name) {
        return classCache.keySet()
                .parallelStream()
                .filter(f -> {
                    if (f == null) {
                        return false;
                    }

                    String[] pathSection = f.split("\\.");
                    if (pathSection.length == 0) {
                        return false;
                    }

                    return name.equals(pathSection[pathSection.length - 1]);
                })
                .collect(Collectors.toList());
    }

    @Override
    public URL getResource(String name) {
        throw new UnsupportedOperationException("The resource is only loaded in memory!");
    }

    @Override
    public Enumeration<URL> getResources(String name) {
        throw new UnsupportedOperationException("The resource is only loaded in memory!");
    }

    @Override
    public InputStream getResourceAsStream(String name) {
        if (name == null) {
            return new ByteArrayInputStream(new byte[0]);
        }

        if (resourcesFilePath == null || name.endsWith(resourcesFilePath)) {
            return AesClassLoader.class.getResourceAsStream(name);
        }


        return new ByteArrayInputStream(getResourceFromCache(name));
    }

    private byte[] getResourceFromCache(String name) {

        String uniformizedName;
        if (name.startsWith("/")) {
            /*
             * To mimic the normal loader, we need the slash before a file name; so we remove it in our case.
             * The values are saved in the case without the slash in the cache.
             */
            uniformizedName = name.replaceFirst("/", "");
        } else {
            uniformizedName = name;
        }

        final byte[] bytes = resourceCache.get(uniformizedName);
        return bytes != null ? bytes : new byte[0];
    }
}
