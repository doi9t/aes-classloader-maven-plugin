/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import ca.watier.maven.plugins.models.ClassType;
import javassist.CtClass;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.AlgorithmParameters;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidParameterSpecException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Mojo(defaultPhase = LifecyclePhase.PREPARE_PACKAGE, name = "encrypt-classes-with-aes")
public class EncryptAesClasses extends AbstractEncryptedActionMojo {

    private final EncryptionContext encryptionContext;
    @Parameter(property = "skipClassesByRegex")
    protected Set<String> skipClassesByRegex;
    @Parameter(property = "skipClassesByType")
    protected Set<ClassType> skipClassesByType;

    @Inject
    public EncryptAesClasses(EncryptionContext encryptionContext) {
        super(encryptionContext);
        this.encryptionContext = encryptionContext;
    }

    @Override
    public void execute() {
        try {
            Cipher cipher = Cipher.getInstance(EncryptionContext.AES_CBC_PKCS_5_PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, encryptionContext.getSecretKey());
            AlgorithmParameters params = cipher.getParameters();

            encryptClassesIfNeeded(cipher, params);
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | BadPaddingException |
                IllegalBlockSizeException | InvalidParameterSpecException e) {
            getLog().error(e);
        }
    }

    private void encryptClassesIfNeeded(Cipher cipher, AlgorithmParameters params) throws IOException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException {
        List<Path> classes = getAllClassesFromOutputDir();
        if (CollectionUtils.isNotEmpty(classes)) {
            long totalEncryptedClass = 0L;
            for (Path classPath : classes) {
                File currentClassFile = classPath.toFile();

                if (needToSkipCurrentClassByRegex(classPath)) {
                    getLog().debug(String.format("Skipping the class by regex [%s]", classPath));
                    continue;
                }

                URI currentUri = classPath.toUri();
                byte[] rawClassBytes = IOUtils.toByteArray(currentUri);

                if (needToSkipCurrentClassByName(classPath)) {
                    getLog().debug(String.format("Skipping the class by name [%s]", classPath));
                    continue;
                } else if (needToSkipCurrentClassByType(rawClassBytes, classPath)) {
                    getLog().debug(String.format("Skipping the class by type [%s]", classPath));
                    continue;
                }

                byte[] ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV(); // 16 bytes
                byte[] encryptedClassBytes = cipher.doFinal(rawClassBytes);

                FileUtils.writeByteArrayToFile(currentClassFile, ArrayUtils.addAll(ivBytes, encryptedClassBytes));

                getLog().debug(String.format("Encrypting class [%s]", classPath));
                String baseName = buildEncryptedClassName(currentClassFile);
                FileUtils.moveFile(currentClassFile, new File(currentClassFile.getParentFile(), baseName));
                getLog().debug(String.format("Renaming the class to [%s]", baseName));

                totalEncryptedClass++;
            }

            if (totalEncryptedClass > 0L) {
                getLog().debug(String.format("Encrypted a total of %s class(es)!", totalEncryptedClass));
            }
        }
    }

    private List<Path> getAllClassesFromOutputDir() throws IOException {
        return Files.walk(Paths.get(outputDirectory.toURI()))
                .filter(p -> p.toString().endsWith(".class"))
                .collect(Collectors.toList());
    }

    private boolean needToSkipCurrentClassByRegex(Path path) throws IOException {
        boolean haveNoRegex = CollectionUtils.isEmpty(this.skipClassesByRegex);

        if (haveNoRegex) {
            return false;
        }

        File file = path.toFile();
        String canonicalPath = file.getCanonicalPath();

        for (Pattern fileNameToSkip : buildRegexPattern()) {
            if (fileNameToSkip.matcher(canonicalPath).find()) {
                return true;
            }
        }

        return false;
    }

    private boolean needToSkipCurrentClassByName(Path classPath) {
        File file = classPath.toFile();
        String basePathAndNameOfClass = Paths.get(outputDirectory.toURI()).relativize(Paths.get(file.toURI())).toString();
        return "ca/watier/maven/plugins/aes/AesClassLoader.class".equals(basePathAndNameOfClass);
    }

    private boolean needToSkipCurrentClassByType(byte[] rawClassBytes, Path classPath) {

        if (CollectionUtils.isEmpty(skipClassesByType)) {
            return false;
        }

        CtClass ctClassFromBytes = createCtClassFromBytes(rawClassBytes);

        if (ctClassFromBytes == null) {
            getLog().warn(String.format("Unable to read the current class [%s], skipping!", classPath));
            return true;
        }

        for (ClassType classType : skipClassesByType) {
            if (classType == ClassType.INTERFACES && ctClassFromBytes.isInterface()) {
                return true;
            } else if (classType == ClassType.ENUMS && ctClassFromBytes.isEnum()) {
                return true;
            } else if (classType == ClassType.ANNOTATIONS && ctClassFromBytes.isAnnotation()) {
                return true;
            }
        }

        return false;
    }

    private String buildEncryptedClassName(File currentClassFile) {
        return FilenameUtils.getBaseName(currentClassFile.getName()) + ENC_CLASS_EXT;
    }

    private Set<Pattern> buildRegexPattern() {
        Set<Pattern> patterns = new HashSet<>();

        for (String classesByRegex : this.skipClassesByRegex) {
            patterns.add(Pattern.compile(classesByRegex));
        }

        return patterns;
    }

    private CtClass createCtClassFromBytes(byte[] rawClassBytes) {
        try {
            return POOL.makeClass(new ByteArrayInputStream(rawClassBytes));
        } catch (IOException e) {
            return null;
        }
    }
}