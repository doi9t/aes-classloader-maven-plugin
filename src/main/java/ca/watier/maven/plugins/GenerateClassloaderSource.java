/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Mojo(defaultPhase = LifecyclePhase.GENERATE_SOURCES, name = "generate-classloader-source")
public class GenerateClassloaderSource extends AbstractEncryptedActionMojo {

    public static final Charset UTF_8 = StandardCharsets.UTF_8;
    private final EncryptionContext encryptionContext;

    @Inject
    public GenerateClassloaderSource(EncryptionContext encryptionContext) {
        super(encryptionContext);
        this.encryptionContext = encryptionContext;
    }

    @Override
    public void execute() {
        try {
            String baseFolder = "ca/watier/maven/plugins/aes";

            InputStream javaSourceAsStream = GenerateClassloaderSource.class.getResourceAsStream("/" + baseFolder + "/AesClassLoader.java");
            String javaSourceAsString = IOUtils.toString(javaSourceAsStream, UTF_8);

            File srcDir = new File(outputGeneratedSourceDirectory, baseFolder);
            File classloaderFile = new File(srcDir, "AesClassLoader.java");

            String base64Salt = Base64.getEncoder().encodeToString(encryptionContext.getSalt());

            HashMap<String, String> templates = new HashMap<>();
            templates.put("private String saltString;", String.format("private String saltString = \"%s\";", base64Salt));
            templates.put("private int iterationCount;", String.format("private int iterationCount = %s;", EncryptionContext.ITERATIONS));
            templates.put("private int keyLength;", String.format("private int keyLength = %s;", EncryptionContext.AES_KEY_LENGTH));
            templates.put("private String keyAlgorithm;", String.format("private String keyAlgorithm = \"%s\";", EncryptionContext.PBKDF_2_WITH_HMAC_SHA_512));
            templates.put("private String transformation;", String.format("private String transformation = \"%s\";", EncryptionContext.AES_CBC_PKCS_5_PADDING));
            templates.put("private String encryptedClassExt;", String.format("private String encryptedClassExt = \"%s\";", ENC_CLASS_EXT));
            templates.put("private String resourcesFilePath;", String.format("private String resourcesFilePath = \"%s\";", "/" + RESOURCES_ENC_PATH_AND_FILE));

            FileUtils.forceMkdir(srcDir);
            FileUtils.writeStringToFile(classloaderFile, edit(javaSourceAsString, templates), UTF_8);
        } catch (IOException e) {
            getLog().error(e);
        }
    }

    private String edit(String javaSourceAsString, HashMap<String, String> replaceMap) {
        String value = javaSourceAsString;
        for (Map.Entry<String, String> replace : replaceMap.entrySet()) {
            value = value.replace(replace.getKey(), replace.getValue());
        }
        return value;
    }
}