/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.AlgorithmParameters;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidParameterSpecException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Mojo(defaultPhase = LifecyclePhase.PREPARE_PACKAGE, name = "encrypt-resources-with-aes")
public class EncryptAesResources extends AbstractEncryptedActionMojo {

    private final EncryptionContext encryptionContext;

    @Parameter(defaultValue = "${project.basedir}/src/main/resources")
    protected File resourceDirectory;

    @Inject
    public EncryptAesResources(EncryptionContext encryptionContext) {
        super(encryptionContext);
        this.encryptionContext = encryptionContext;
    }

    @Override
    public void execute() {
        try {
            Cipher cipher = Cipher.getInstance(EncryptionContext.AES_CBC_PKCS_5_PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, encryptionContext.getSecretKey());
            AlgorithmParameters params = cipher.getParameters();

            encryptResourceIfNeeded(cipher, params);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidParameterSpecException e) {
            getLog().error(e);
        }
    }

    private void encryptResourceIfNeeded(Cipher cipher, AlgorithmParameters params) throws InvalidParameterSpecException {
        try {
            final List<String> projectResourceBasePathFiles = getProjectResourceBasePathFiles();
            final Path basePath = Paths.get(outputDirectory.toURI());
            final List<Path> resourceFiles = Files.walk(basePath)
                    .filter(p -> {
                        final String relativizePath = basePath.relativize(p).toString();
                        return projectResourceBasePathFiles.contains(relativizePath);
                    }).collect(Collectors.toList());

            if (CollectionUtils.isEmpty(resourceFiles)) {
                return;
            }

            try (FileOutputStream fos = new FileOutputStream(new File(outputDirectory, RESOURCES_ENC_PATH_AND_FILE))) {

                fos.write(params.getParameterSpec(IvParameterSpec.class).getIV()); // 16 bytes

                try (CipherOutputStream cos = new CipherOutputStream(fos, cipher)) {
                    try (ZipOutputStream zipOut = new ZipOutputStream(cos)) {
                        for (Path currentResourcePath : resourceFiles) {
                            final File currentResourceFile = currentResourcePath.toFile();

                            if (currentResourceFile.isDirectory()) {
                                continue;
                            }

                            getLog().debug(String.format("Adding [%s] to the resource zip", currentResourceFile.toPath()));

                            ZipEntry zipEntry = new ZipEntry(basePath.relativize(currentResourcePath).toString());
                            zipOut.putNextEntry(zipEntry);
                            IOUtils.copy(new FileInputStream(currentResourceFile), zipOut);

                            deleteFileAndParentDirIfEmpty(currentResourceFile);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> getProjectResourceBasePathFiles() throws IOException {
        final Path basePath = Paths.get(resourceDirectory.toURI());
        return Files
                .walk(basePath)
                .filter(path -> path.toFile().isFile())
                .map(path -> basePath.relativize(path).toString())
                .collect(Collectors.toList());
    }

    private void deleteFileAndParentDirIfEmpty(File currentResourceFile) throws IOException {
        FileUtils.forceDelete(currentResourceFile);

        final File parentFile = currentResourceFile.getParentFile();

        if (ArrayUtils.isEmpty(parentFile.list())) {
            FileUtils.forceDelete(parentFile);
        }
    }
}