/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Optional;
import java.util.regex.Pattern;

@Named
@Singleton
public class EncryptionContext {

    public static final String AES_CBC_PKCS_5_PADDING = "AES/CBC/PKCS5Padding";
    public static final String PBKDF_2_WITH_HMAC_SHA_512 = "PBKDF2WithHmacSHA512";
    public static final int AES_KEY_LENGTH = 256; // 32 bytes
    public static final int ITERATIONS = 65536;

    private static final String GENERATED_KEY_FILE = "aes-classloader.key";

    private static final Pattern MAVEN_VARIABLE_PATTERN = Pattern.compile("\\$\\{*.+\\}");

    private final SecretKey secretKey;
    private final byte[] salt;
    private final File projectBasedir;

    @Inject
    public EncryptionContext(MojoExecution mojoExecution, MavenProject mavenProject) throws NoSuchAlgorithmException, InvalidKeySpecException, MojoExecutionException {
        projectBasedir = mavenProject.getBasedir();
        salt = generateSalt();

        String key = fetchKeyFromPom(mojoExecution);

        SecretKeyFactory factory = SecretKeyFactory.getInstance(PBKDF_2_WITH_HMAC_SHA_512);
        KeySpec spec = new PBEKeySpec(key.toCharArray(), salt, ITERATIONS, AES_KEY_LENGTH);
        SecretKey tmp = factory.generateSecret(spec);

        secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
    }

    private byte[] generateSalt() {
        try {
            SecureRandom random = SecureRandom.getInstanceStrong();
            byte[] salt = new byte[64];
            random.nextBytes(salt);
            return salt;
        } catch (NoSuchAlgorithmException e) {
            return new byte[0];
        }
    }

    private String fetchKeyFromPom(MojoExecution mojoExecution) throws MojoExecutionException {
        String key = extractStringValueFromDom(mojoExecution, "key");
        String keyB64 = extractStringValueFromDom(mojoExecution, "keyB64");

        if (key != null && keyB64 != null) {
            throw new MojoExecutionException("The `key` & `keyB64` cannot be applied at the same time!");
        } else if (key != null) {
            return key;
        } else if (keyB64 != null) {
            if (!Base64.isBase64(keyB64)) {
                throw new MojoExecutionException("The `keyB64` is not recognized as base64!");
            }

            return new String(Base64.decodeBase64(keyB64));
        } else {
            try {
                String generatedKey = RandomStringUtils.random(16);
                generatePasswordAndSaveToFile(generatedKey);
                return generatedKey;
            } catch (IOException e) {
                throw new MojoExecutionException("Unable to save the file containing the key!", e);
            }
        }
    }

    private String extractStringValueFromDom(MojoExecution mojoExecution, String keyName) {
        Xpp3Dom domKey = Optional.ofNullable(mojoExecution)
                .map(MojoExecution::getConfiguration)
                .map(conf -> conf.getChild(keyName))
                .orElse(null);

        String rawValue = Optional.ofNullable(domKey).map(Xpp3Dom::getValue).orElse("");
        return StringUtils.isBlank(rawValue) || MAVEN_VARIABLE_PATTERN.matcher(rawValue).find() ? null : rawValue;
    }

    private void generatePasswordAndSaveToFile(String generatedKey) throws IOException {
        FileUtils.writeByteArrayToFile(new File(projectBasedir, GENERATED_KEY_FILE), Base64.encodeBase64(generatedKey.getBytes()));
    }

    public byte[] getSalt() {
        return Arrays.copyOf(salt, salt.length);
    }

    public SecretKey getSecretKey() {
        return secretKey;
    }
}
