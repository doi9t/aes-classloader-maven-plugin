/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.maven.plugins;

import javassist.ClassPool;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Parameter;

import javax.inject.Inject;
import java.io.File;

public abstract class AbstractEncryptedActionMojo extends AbstractMojo {
    protected static final String RESOURCES_ENC_PATH_AND_FILE = "resources.resenc";
    protected static final String ENC_CLASS_EXT = ".enc";

    protected static final ClassPool POOL = ClassPool.getDefault();
    @Parameter(property = "key")
    @SuppressWarnings("unused")
    protected String key;

    @Parameter(property = "keyB64")
    @SuppressWarnings("unused")
    protected String keyB64;

    @Parameter(defaultValue = "${project.build.outputDirectory}")
    protected File outputDirectory;

    @Parameter(defaultValue = "${project.build.directory}/generated-sources")
    protected File outputGeneratedSourceDirectory;

    @Inject
    public AbstractEncryptedActionMojo(EncryptionContext encryptionContext) {
    }
}
